/*
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
*/
package com.corecanarias.solfege.android.games;

import junit.framework.TestCase;

public class TestMelodicIntervals extends TestCase {
	private MelodicIntervals game;
	private RandomProxy random;
	private MidiProxy midi;

	@Override
	protected void setUp() throws Exception {
		midi = new MidiProxy();
		game = new MelodicIntervals(midi);
		random = new RandomProxy();
		game.setRandom(random);
	}
	
	public void testAscendingSecond() {
		initAscendingSecondInterval(MelodicIntervals.MAJOR);
		boolean res = game.test(MelodicIntervals.MAJOR);
		assertTrue(res);
		assertEquals(10, midi.getNotes().get(0).intValue());
		assertEquals(12, midi.getNotes().get(1).intValue());
	}

	public void testDescendingSecond() {
		initAscendingSecondInterval(MelodicIntervals.MAJOR, MelodicIntervals.DIR_DESCENDING);
		boolean res = game.test(MelodicIntervals.MAJOR);
		assertTrue(res);
		assertEquals(10, midi.getNotes().get(0).intValue());
		assertEquals(8, midi.getNotes().get(1).intValue());		
	}

	private void initAscendingSecondInterval(int interv) {
		initAscendingSecondInterval(interv, MelodicIntervals.DIR_ASCENDING);
	}

	private void initAscendingSecondInterval(int interv, int direction) {
		midi.clear();
		random.clear();
		random.addNextValue(10);
		
		random.addNextValue(interv -1);
		
		game.setDirection(direction);
		game.setInterval(MelodicIntervals.INT_SECOND);
		game.newInterval();
	}
	
	public void testAscendingSecondFail() {
		initAscendingSecondInterval(MelodicIntervals.MAJOR);
		boolean res = game.test(MelodicIntervals.MINOR);
		assertFalse(res);
		assertEquals(10, midi.getNotes().get(0).intValue());
		assertEquals(12, midi.getNotes().get(1).intValue());		
	}
	
	public void testGetCurrentInterval() {
		initAscendingSecondInterval(MelodicIntervals.MINOR);
		assertEquals(MelodicIntervals.MINOR, game.getCurrentInterval());
	}
	
	public void testGetCurrentMajorInterval() {
		initAscendingSecondInterval(MelodicIntervals.MAJOR);
		assertEquals(MelodicIntervals.MAJOR, game.getCurrentInterval());		
	}

	public void testGetLastResponse() {
		initAscendingSecondInterval(MelodicIntervals.MAJOR);
		game.test(MelodicIntervals.MAJOR);
		assertEquals(MelodicIntervals.MAJOR, game.getLastResponse());		
	}

	public void testGetLastResponseFail() {
		initAscendingSecondInterval(MelodicIntervals.MAJOR);
		game.test(MelodicIntervals.MINOR);
		assertEquals(MelodicIntervals.MINOR, game.getLastResponse());		
	}
}
