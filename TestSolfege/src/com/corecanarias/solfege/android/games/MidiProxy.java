/*
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
*/
package com.corecanarias.solfege.android.games;

import java.util.ArrayList;
import java.util.List;

import com.corecanarias.solfege.android.midi.Midi;

public class MidiProxy implements Midi {
	private List<Integer> notes;
	
	public MidiProxy() {
		clear();
	}
	
	public void playNote(int note) {
		notes.add(note);
	}

	public void release() {
		
	}
	
	public void clear() {
		notes = new ArrayList<Integer>();
	}
	
	public List<Integer> getNotes() {
		return notes;
	};
}
