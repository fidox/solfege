package com.corecanarias.solfege.android.midi;

public interface Midi {

	void playNote(int note);

	void release();
}
