package com.corecanarias.solfege.android.midi;

import com.corecanarias.solfege.android.midi.Midi;

import midi.driver.MidiPlayer;

public class MidiImpl implements Midi {
	private MidiPlayer player;

	public MidiImpl() {
		player = new MidiPlayer();
		player.prepare();
		player.createMidiStream();
	}
	
	public void playNote(int note) {
		player.setNoteOn(0, note, 100);
		sleep(1000);
	}

	public void release() {
	
		if(player != null) {
			player.release();
		}
	}
	
	private void sleep(long s) {
		try {
			Thread.sleep(s);
		} catch (InterruptedException e) {
		}
	}
}
