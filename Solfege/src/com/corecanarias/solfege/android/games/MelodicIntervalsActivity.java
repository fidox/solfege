/*
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
*/
package com.corecanarias.solfege.android.games;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.corecanarias.solfege.android.R;
import com.corecanarias.solfege.android.midi.Midi;
import com.corecanarias.solfege.android.midi.MidiImpl;

public class MelodicIntervalsActivity extends Activity {
	final static int NOTE_A0 = 21;
	private TextView msg;
	private TextView textResults;
	private boolean countedForResults = false;
	private int notes = 0;
	private int fails = 0;
	private Midi midi;
	private MelodicIntervals melodicIntervals;
	private Vibrator vibrator;
	
	public static final String LOG = "Solfege";

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.melodic_intervals);

        Bundle extras = getIntent().getExtras();
			
        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		midi = new MidiImpl();
		melodicIntervals = new MelodicIntervals(midi);
		melodicIntervals.setInterval((Integer)extras.get("interval") + 2);
		melodicIntervals.setDirection(Integer.valueOf((String)extras.get("direction")));
	
		Log.d("SOLFEGE", "Interval: " + melodicIntervals.getInterval() + " " + melodicIntervals.getDirection());

		msg = (TextView)findViewById(R.id.message);
		textResults = (TextView)findViewById(R.id.results);
		Button buttonNew = (Button)findViewById(R.id.buttonNew);
		buttonNew.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				nextNote();
			}
		});
		Button buttonRepeat = (Button)findViewById(R.id.repeat);		
		buttonRepeat.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				repeat();
			}
		});
		Button buttonA = (Button)findViewById(R.id.button1);		
		buttonA.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				if(!melodicIntervals.test(MelodicIntervals.MINOR)) {
					notifyError();
				} else {
					countResults(false);
					nextNote();
				}
			}
		});
		Button buttonB = (Button)findViewById(R.id.button2);
		buttonB.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				if(!melodicIntervals.test(MelodicIntervals.MAJOR)) {
					notifyError();
				} else {
					countResults(false);
					nextNote();
				}
			}
		});
    }

    
    @Override
    protected Dialog onCreateDialog(int id) {
    	if(id == 0) {
        	final Dialog alertDialog = new Dialog(this);
        	alertDialog.setContentView(R.layout.error_dialog);
        	alertDialog.setCancelable(false);    
        	alertDialog.setTitle("Wrong answer");
    		Button button = (Button)alertDialog.findViewById(R.id.err_dlg_button1);
    		button.setOnClickListener(new OnClickListener() {
				
				public void onClick(View arg0) {
					playClickedNote(melodicIntervals.getLastResponse());
				}
			});
    		button = (Button)alertDialog.findViewById(R.id.err_dlg_button2);
    		button.setOnClickListener(new OnClickListener() {
				
				public void onClick(View arg0) {
					playClickedNote(melodicIntervals.getCurrentInterval());				
				}
			});
    		button = (Button)alertDialog.findViewById(R.id.err_dlg_button3);
    		button.setOnClickListener(new OnClickListener() {
				
				public void onClick(View arg0) {
					nextNote();
					alertDialog.dismiss();
				}
			});
        	return alertDialog;
    	}
    	return null;
    }
    
    private void countResults(boolean fail) {
    	if(!countedForResults) {
    		countedForResults = true;		
    		notes++;
    		if(fail) {
    			fails++;
    		}
    		textResults.setText("Total notes: " + notes + " total errors: " + fails);
    	}
    }
    
    private void repeat() {
    
    	new Thread(new Runnable() {

			public void run() {
				melodicIntervals.play();
			}    		
    	}).start();		
    }
    
	private void playClickedNote(final int i) {
    	new Thread(new Runnable() {

			public void run() {
				melodicIntervals.playSelected(i);
			}    		
    	}).start();		
	}

	private void notifyError() {
		countResults(true);
		Log.d(LOG, "error");
		vibrator.vibrate(500);
		message("Error, try it again");
		showDialog(0);
	}

    private void message(String msg) {
    	this.msg.setText(msg);
    }
    
    private void nextNote() {
    	countedForResults = false;
		message("Identify the interval");
    	new Thread(new Runnable() {

			public void run() {
				melodicIntervals.newInterval();
			}    		
    	}).start();
    }
    
	@Override
	protected void onDestroy() {
		midi.release();
		super.onDestroy();
	}
}
