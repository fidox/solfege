/*
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
*/
package com.corecanarias.solfege.android.games;

import com.corecanarias.solfege.android.midi.Midi;
import com.corecanarias.solfege.android.utils.Random;
import com.corecanarias.solfege.android.utils.RandomImpl;

public class MelodicIntervals {
	public static final int DIR_ASCENDING = 1;
	public static final int DIR_DESCENDING = 2;
	public static final int INT_SECOND = 2;
	public static final int MINOR = 1;
	public static final int MAJOR = 2;
	
	private Midi midi;
	private int direction = DIR_ASCENDING;
	private int interval = INT_SECOND;
	private Random random;
	
	private int startNote = 30;
	private int endNote = 100;
	private int currentNote;
	private int secondNote;
	private int lastResponse;
	
	public MelodicIntervals(Midi midiProxy) {
		midi = midiProxy;
		random = new RandomImpl();
	}

	public void setRandom(Random random) {
		this.random = random;
	}
	
	public void setDirection(int dir) {
		direction = dir;
	}

	public int getDirection() {
		return direction;
	}
	
	public void setInterval(int inter) {
		interval = inter;
	}

	public int getInterval() {
		return interval;
	}
	
	public void newInterval() {
		currentNote = random.nextInt(startNote, endNote);
		secondNote = random.nextInt(0, interval) + 1;
		play();
	}

	public void play() {
		midi.playNote(currentNote);	
		if(direction == DIR_ASCENDING) {
			midi.playNote(secondNote + currentNote);
		} else {			
			midi.playNote(currentNote - secondNote);
		}
	}
	
	public void playSelected(int interval) {
		midi.playNote(currentNote);
		if(direction == DIR_ASCENDING) {
			midi.playNote(currentNote + interval);							
		} else {
			midi.playNote(currentNote - interval);										
		}
	}
	
	public boolean test(int response) {
		lastResponse = response;
		return response == secondNote;
	}
	
	public int getLastResponse() {
		return lastResponse;
	}

	public int getCurrentInterval() {
		return secondNote;
	}
}
