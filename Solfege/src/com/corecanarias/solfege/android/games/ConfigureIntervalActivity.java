/*
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
*/
package com.corecanarias.solfege.android.games;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.corecanarias.solfege.android.R;
import com.corecanarias.solfege.android.utils.ListItem;

public class ConfigureIntervalActivity extends ListActivity {
	private ListItem[] intervals = {
			ListItem.newItem("Seconds", 2),
			ListItem.newItem("Thirds", 3),
			ListItem.newItem("Fourths and fifths", MelodicIntervalsActivity.class),
			ListItem.newItem("Sixths", MelodicIntervalsActivity.class),
			ListItem.newItem("Sevenths", MelodicIntervalsActivity.class),
			ListItem.newItem("Ninths", MelodicIntervalsActivity.class),
			ListItem.newItem("Tenths", MelodicIntervalsActivity.class),
			ListItem.newItem("Tritone and sevenths", MelodicIntervalsActivity.class),
			ListItem.newItem("Fourths, fifths and octave", MelodicIntervalsActivity.class),
			ListItem.newItem("Sevenths and ninths", MelodicIntervalsActivity.class),
			ListItem.newItem("Seconds and thirds", MelodicIntervalsActivity.class),
			ListItem.newItem("Sixths and sevenths", MelodicIntervalsActivity.class),
			ListItem.newItem("Second to octave", MelodicIntervalsActivity.class),
			ListItem.newItem("Second to tenth", MelodicIntervalsActivity.class),
			};
	private RadioGroup radio;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configure_interval);

        radio = (RadioGroup)findViewById(R.id.radio);
        setListAdapter(new ArrayAdapter<ListItem>(this, android.R.layout.simple_list_item_1, intervals));
    }
    
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	
    	RadioButton activeDir = (RadioButton)findViewById(radio.getCheckedRadioButtonId());
    	
    	Intent i = new Intent(this, MelodicIntervalsActivity.class);
    	i.putExtra("interval", position);
    	i.putExtra("direction", (String)activeDir.getTag());
    	startActivity(i);
    }
}
