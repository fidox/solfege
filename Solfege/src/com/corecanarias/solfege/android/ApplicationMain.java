/*
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
*/
package com.corecanarias.solfege.android;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.corecanarias.solfege.android.games.ConfigureIntervalActivity;
import com.corecanarias.solfege.android.utils.ListItem;

public class ApplicationMain extends ListActivity {	
	public static final String LOG = "Solfege";

	private ListItem[] menuItems = {
			new ListItem("Melodic intervals", ConfigureIntervalActivity.class),
			new ListItem("Armonic intervals", ConfigureIntervalActivity.class),
			new ListItem("Chords", ConfigureIntervalActivity.class)
			};
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new ArrayAdapter<ListItem>(this, android.R.layout.simple_list_item_1, menuItems));        
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	ListItem val = menuItems[position];
    	Intent i = new Intent(this, (Class)val.getValue());
    	startActivity(i);
    }
    
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
