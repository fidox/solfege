package midi.driver;

/*
 *
 * Created by: Byron Yau
 * Date: 16/6/2010
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL or COMMERCIAL purposes and
 * without fee is hereby granted. 
 */



import java.io.FileDescriptor;

public class MidiPlayer {

	static {
		System.loadLibrary("midiplayer");
	}

	private final static String TAG = "MidiPlayer";
	private int mNativeContext; // accessed by native methods

	public MidiPlayer() {
		native_setup();
	}
	
	public final int setNoteOn(int channel, int note, int velocity){
		return setNoteOn(0, channel, note, velocity);
	}
	
	public final int setNoteOff(int channel, int note, int velocity) {
		return setNoteOff(0, channel, note, velocity);
	}
	
	public final int setNoteAfterTouch(int channel, int note, int afterTouchValue){
		return setNoteAfterTouch(0, channel, note, afterTouchValue);
	}
	
	private native final void native_setup();
	private native final void native_release();
	public native final int setDataSource(String filename);
	public native final int setDataSource(FileDescriptor fd, long offset, long length);
	public native final int prepare();
	public native final int prepareAsync();
	public native final void start();
	public native final void stop();
	public native final int seekTo(int position);
	public native final void pause();
	public native final boolean isPlaying();
	public native final int getCurrentPosition();
	public native final int getDuration();
	public native final void release();
	public native final void reset();
	public native final void setLooping(int loop);
	public native final int createMidiStream();
	public native final int writeStreamData(byte[] pStreamData, long count);
	public native final int putStreamData(byte[] pStreamData, long count);
	public native final void resume(int streamID);

	public native final int setProgramChange(int channel, int program);
	public native final int setChannelAfterTouch(int channel, int afterTouchValue);
	public native final int setPitchBend(int channel, int pitchValue);
	public native final int setMainVolume(int channel, int volume);
	public native final int setBalance(int channel, int balance);
	public native final int setPan(int channel, int pan);
	public native final int setBank(int channel, int bank);
	public native final int setNoteOn(int time, int channel, int note, int velocity);
	public native final int setNoteOff(int time, int channel, int note, int velocity);
	public native final int setNoteAfterTouch(int time, int channel, int note, int afterTouchValue);
}
